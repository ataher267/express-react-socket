import './App.css';
import { useEffect, useState } from 'react';
import io from 'socket.io-client';
const socket = io.connect('http://localhost:4000');
function App() {
  const [room, setRoom] = useState('');
  const [msg, setMsg] = useState('');
  const [rcvMsg, setRcvMsg] = useState('');
  const sendMessage = async (e) => {
    socket.emit('send_message', { message: msg, room });
  };
  const joinRoom = () => {
    if (room !== '') {
      socket.emit('join_room', room);
      console.log(room);
    }
  };
  useEffect(() => {
    socket.on('receive_message', (data) => {
      console.log('jfdkjd');
      setRcvMsg(data.message);
    });
  }, [socket]);
  return (
    <div className="App">
      <input
        type="text"
        placeholder="meet id"
        onChange={({ target: { value } }) => setRoom(value)}
      />
      <input type="button" onClick={joinRoom} value="Join" />
      <hr />
      <input type="text" onChange={(e) => setMsg(e.target.value)} />
      <input type="button" onClick={sendMessage} value="Send" />
      <hr />
      message: {rcvMsg}
    </div>
  );
}

export default App;
