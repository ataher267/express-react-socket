const express = require('express');
const app = express();
const { createServer } = require('http');
const { Server } = require('socket.io');
const middlewires = [require('cors')()];
app.use(middlewires);
const server = createServer(app);
const io = new Server(server, {
  cors: {
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
  },
});
// global.io = io;
io.on('connection', (socket) => {
  // console.log(socket.id);

  socket.on('join_room', (data) => {
    console.log(data);
    socket.join(data);
  });
  socket.on('send_message', (rec) => {
    console.log(rec);
    socket.to(rec.room).emit('receive_message', rec);
  });

  // socket.on('send_message', (data) => {
  //   // console.log(data);
  //   socket.broadcast.emit(
  //     'receive_message',
  //     (data = { ...data, broadcast: 'for all' })
  //   );
  // });
});
const PORT = 4000;
server.listen(PORT, () =>
  console.log(`Alhamdu lillah, Server is listerning on port : ${PORT}`)
);
